package com.e16din.simplesocials.managers;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.e16din.simplesocials.core.interfaces.BaseSocialManger;
import com.e16din.simplesocials.core.interfaces.SuccessListener;
import com.e16din.simplesocials.core.utils.ImageLoader;
import com.e16din.simplesocials.core.utils.ImageLoader.OnImageLoaderListener;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.UiLifecycleHelper;

public class FacebookManager extends BaseSocialManger {
	private UiLifecycleHelper uiHelper = null;
	private static SuccessListener authListener = null;

	private StatusCallback authCallback = new StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (exception != null) {
				Log.i("debug_fb", "exception: " + exception.toString());
				exception.printStackTrace();
				if (authListener != null && exception instanceof FacebookOperationCanceledException) {
					authListener.onCancel();
					authListener = null;
					return;
				}
			}

			if (state.isOpened()) {
				setToken(session.getAccessToken());
				if (authListener != null){
					authListener.onSuccess(FacebookManager.this, getToken());
					authListener = null;
				}

				Log.i("debug_fb", "Logged in...");
			} else if (state.isClosed()) {
				if (authListener != null){
					authListener.onSuccess(FacebookManager.this, getToken());
					authListener = null;
				}
				Log.i("debug_fb", "Logged out...");
			}
		}
	};

	private StatusCallback getAuthCallback() {
		return authCallback;
	}

	public FacebookManager(Activity activity, String appId) {
		super(activity);

		Settings.setApplicationId(appId);
		uiHelper = new UiLifecycleHelper((Activity) activity, getAuthCallback());
		uiHelper.onCreate(null);
	}

	@Override
	public String getName() {
		return "Facebook";
	}

	@Override
	public void login(SuccessListener listener) {
		super.login(listener);
		Log.d("debug_fb", "login fbbbbb");
		authListener = listener;

		Session session = Session.getActiveSession();
		try {
			session.openForPublish(new Session.OpenRequest((Activity) getContextActivity())
					.setPermissions(Arrays.asList("publish_actions"))
					.setLoginBehavior(
							isAuth() ? SessionLoginBehavior.SSO_WITH_FALLBACK : SessionLoginBehavior.SUPPRESS_SSO)
					.setCallback(getAuthCallback()));
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
			Session.setActiveSession(new Builder((Activity) getContextActivity()).build());
			session = Session.getActiveSession();
			session.openForPublish(new Session.OpenRequest((Activity) getContextActivity())
					.setPermissions(Arrays.asList("publish_actions"))
					.setLoginBehavior(
							isAuth() ? SessionLoginBehavior.SSO_WITH_FALLBACK : SessionLoginBehavior.SUPPRESS_SSO)
					.setCallback(getAuthCallback()));
		}
	}

	@Override
	public void logout(final SuccessListener listener) {
		super.logout(listener);

		Session session = Session.getActiveSession();
		if (session != null) {
			session.closeAndClearTokenInformation();
		} else {
			Session session2 = Session.openActiveSession((Activity) getContextActivity(), false, null);
			if (session2 != null)
				session2.closeAndClearTokenInformation();
		}
		Session.setActiveSession(null);

		uiHelper.onPause();
		uiHelper.onStop();
		uiHelper.onDestroy();

		uiHelper = null;
		uiHelper = new UiLifecycleHelper((Activity) getContextActivity(), getAuthCallback());
		uiHelper.onCreate(null);
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
		uiHelper.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(activity, requestCode, resultCode, data);

		Log.d("debug_fb", "resultCode: " + resultCode);
		if (resultCode == Activity.RESULT_CANCELED && authListener != null) {
			authListener.onCancel();
			authListener = null;
		}
	}

	@Override
	public void onResume(Activity activity) {
		super.onResume(activity);
		uiHelper.onResume();
		Log.d("debug_fb", "onResume");
	}

	@Override
	public void onDestroy(Activity activity) {
		super.onDestroy(activity);
		uiHelper.onDestroy();
	}

	@Override
	public void onStop(Activity activity) {
		super.onStop(activity);
		uiHelper.onStop();
	}

	@Override
	public void onPause(Activity activity) {
		super.onPause(activity);
		uiHelper.onPause();
	}

	@Override
	public void onSaveInstanceState(Activity activity, Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void postPhoto(final String message, final String postUrl, final String mediaUrl,
			final SuccessListener listener) {
		if (TextUtils.isEmpty(Session.getActiveSession().getAccessToken())) {
			loginBeforeSharing(false, message, postUrl, mediaUrl, listener);
			return;
		} else
			setToken(Session.getActiveSession().getAccessToken());

		Log.d("debug_fb", "postPhoto");
		new ImageLoader(mediaUrl, new OnImageLoaderListener() {
			@Override
			public void onCompleted(byte[] data, Bitmap bitmap) {
				Bundle params = new Bundle();
				params.putString("name", message + " " + postUrl);
				params.putByteArray("picture", data);

				executeRequest("me/photos", params, getPostCallback(false, message, postUrl, mediaUrl, listener));
			}
		}).execute();
	}

	private void executeRequest(String method, Bundle params, final Request.Callback callback) {
		Log.d("debug_fb", "executeRequest");
		Request request = new Request(Session.getActiveSession(), method, params, HttpMethod.POST, callback);
		new RequestAsyncTask(request).execute();
	}

	@Override
	protected void postVideo(String message, final String postUrl, String mediaUrl, final SuccessListener listener) {
		Log.d("debug_fb", "postVideo");
		if (TextUtils.isEmpty(Session.getActiveSession().getAccessToken())) {
			loginBeforeSharing(true, message, postUrl, mediaUrl, listener);
			return;
		} else
			setToken(Session.getActiveSession().getAccessToken());

		Bundle params = new Bundle();
		params.putString("description", message + " " + postUrl);
		params.putString("link", "http://www.youtube.com/watch?v=" + mediaUrl);

		executeRequest("me/feed", params, getPostCallback(true, message, postUrl, mediaUrl, listener));
	}

	private Callback getPostCallback(final boolean isVideo, final String message, final String postUrl,
			final String mediaUrl, final SuccessListener listener) {
		Log.d("debug_fb", "getPostCallback+isVideo: "+isVideo);
		return new Request.Callback() {
			public void onCompleted(Response response) {
				JSONObject graphResponse = new JSONObject();
				Log.d("debug_fb", "response: " + response.toString());

				if (response.getGraphObject() != null)
					graphResponse = response.getGraphObject().getInnerJSONObject();

				try {
					graphResponse.getString("id");
					listener.onSuccess(FacebookManager.this, postUrl.substring(postUrl.lastIndexOf("/") + 1));
				} catch (JSONException e) {
					e.printStackTrace();
					authListener = new SuccessListener() {
						@Override
						public void onSuccess(BaseSocialManger manager, String supportStr) {
							if (isVideo)
								postVideo(message, postUrl, mediaUrl, listener);
							else
								postPhoto(message, postUrl, mediaUrl, listener);
						}

						@Override
						public void onFail(String error, BaseSocialManger manager) {
							Log.e("debug_fb", "onFail: " + listener);
							if (listener != null) {
								listener.onFail(error, manager);
							}
						}

						@Override
						public void onCancel() {
							Log.e("debug_fb", "onCancel: " + listener);
							if (listener != null) {
								listener.onCancel();
							}
						}
					};

					login(authListener);
				}

				FacebookRequestError error = response.getError();
				if (error != null) {
					Log.e("debug_fb", "error: " + error.toString());
					listener.onFail(error.toString(), FacebookManager.this);
				}
			}
		};
	}
}
